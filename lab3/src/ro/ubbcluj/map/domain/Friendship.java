package ro.ubbcluj.map.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

public class Friendship extends Entity<Pair<Long>> { //deja le am in pair(id1,id2)
    LocalDate date;

    public Friendship(Pair<Long> id) {
        this.id = id;
        this.date= LocalDate.now();
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Friendship{" +
                "id1=" + super.id.getFirst() + " " +
                "id2=" + super.id.getSecond() +
                ", date=" + date +
                '}' + "\n";
    }

}

