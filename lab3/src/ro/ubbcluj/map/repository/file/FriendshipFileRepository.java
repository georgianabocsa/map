package ro.ubbcluj.map.repository.file;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.validators.Validator;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class FriendshipFileRepository extends AbstractFileRepository<Pair<Long>, Friendship> {

    public FriendshipFileRepository(String fileName, Validator<Friendship> validator) {
        super(fileName, validator);
    }

    @Override
    protected Friendship extractEntity(List<String> attributes) {
        Friendship friendship = new Friendship(new Pair<>(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1))));
        friendship.setDate(LocalDate.parse(attributes.get(2), DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        return friendship;
    }

    @Override
    protected String createEntityAsString(Friendship entity) {
        String line = entity.getId().getFirst() + ";" + entity.getId().getSecond() + ";" +
                entity.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        return line;
    }
}

