package ro.ubbcluj.map.repository.file;

import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.validators.Validator;

import java.util.List;

//lista de pv la inceput vida nu tin in fisierul cu prieteni si prieteniile(alt fisier prietenii)

public class UserFileRepository extends AbstractFileRepository<Long, User> {

    public UserFileRepository(String fileName, Validator<User> validator) {
        super(fileName, validator);
    }

    @Override
    protected User extractEntity(List<String> attributes) {
        User user = new User(attributes.get(1), attributes.get(2));
        user.setId(Long.parseLong(attributes.get(0)));
        return user;
    }

    @Override
    protected String createEntityAsString(User entity) {
        String line = entity.getId() + ";" + entity.getFirstName() + ";" + entity.getLastName();
        return line;
    }
}
