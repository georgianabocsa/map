package ro.ubbcluj.map.repository.database;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.validators.Validator;

import java.sql.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class FriendshipDataBaseRepository implements Repository<Pair<Long>, Friendship>{
    private String url;
    private String username;
    private String password;
    private Validator<Friendship> validator;

    public FriendshipDataBaseRepository(String url, String username, String password, Validator<Friendship> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }


    @Override
    public Friendship findOne(Pair<Long> longPair) {
        if (longPair == null)
            throw new IllegalArgumentException("id must be not null");

        String sql = "select id1,id2,friendship_date from friendships where id1 = ? AND id2=?";
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setLong(1, longPair.getFirst());
            ps.setLong(2, longPair.getSecond());
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                LocalDate date = resultSet.getObject(3,LocalDate.class);
                Friendship friendship = new Friendship(new Pair<>(id1,id2));
                friendship.setDate(date);
                return friendship;

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Friendship> findAll() {
        Set<Friendship> friendships = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendships");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                LocalDate date = resultSet.getObject(3,LocalDate.class);
                Friendship friendship = new Friendship(new Pair<>(id1,id2));
                friendship.setDate(date);
                friendships.add(friendship);
            }
            return friendships;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friendships;
    }

    @Override
    public Friendship save(Friendship entity) {
        if (entity == null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);
        String sql = "insert into friendships (id1, id2, friendship_date) values (?, ?,?)";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, entity.getId().getFirst());
            ps.setLong(2, entity.getId().getSecond());
            ps.setDate(3, Date.valueOf(entity.getDate()));

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Friendship delete(Pair<Long> longPair) {
        if (longPair == null)
            throw new IllegalArgumentException("id must be not null");

        Friendship friendship = findOne(longPair);
        String sql = "delete from friendships where id1 = ? AND id2 = ?";
        int rowCount = 0;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, longPair.getFirst());
            ps.setLong(2, longPair.getSecond());
            rowCount = ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (rowCount > 0) {
            return friendship;
        }

        return null;
    }

    @Override
    public Friendship update(Friendship entity) {
        return null;
    }
}
