package ro.ubbcluj.map.service;

import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.validators.ServiceException;

import java.util.*;

//userRepo.findAll-nodurile getFriends(lista prieteni useri)-muchiile
public class ConnectionService {
    private Repository<Long, User> userRepository;
    private ArrayList<ArrayList<User>> components = new ArrayList<>();

    public ConnectionService(Repository<Long, User> userRepository) {
        this.userRepository = userRepository;
    }


    private void DFSUtil(User user,  Map<User,Boolean> visited, ArrayList<User> al) {
        visited.put(user,true);
        al.add(user);
        for (User x : user.getFriends()) {
            if (visited.get(x)==null)
                DFSUtil(x, visited, al);
        }
    }

    private void DFS() {
        Map<User,Boolean> visited = new HashMap<>();
        Iterable<User> users = userRepository.findAll();
        for (User user : users) {
            ArrayList<User> al = new ArrayList<>();
            if (visited.get(user)==null){
                DFSUtil(user, visited, al);
                components.add(al);
                System.out.println();
            }
        }
    }

    public ArrayList<ArrayList<User>> connectedComponents() {
        DFS();
        if(components.size()==0)
            throw new ServiceException("There are no users");
        return components;
    }

    public ArrayList<User>  theMostSociableCommunity() {
        ArrayList<ArrayList<User>> components = connectedComponents();
        int maxVertices = Integer.MIN_VALUE;
        ArrayList<User> socialCommunity = new ArrayList<>();
        for (ArrayList<User> community : components) {
            if(community.size()>maxVertices){
                maxVertices=community.size();
                socialCommunity=community;
            }
        }
        return  socialCommunity;
    }

}
