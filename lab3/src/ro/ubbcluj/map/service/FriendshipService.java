package ro.ubbcluj.map.service;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.validators.ServiceException;

public class FriendshipService {
    private Repository<Long, User> userRepository;
    private Repository<Pair<Long>, Friendship> friendshipRepository;

    public FriendshipService(Repository<Long, User> userRepository, Repository<Pair<Long>, Friendship> friendshipRepository) {
        this.userRepository = userRepository;
        this.friendshipRepository = friendshipRepository;
        //updateUsersFriendships();
    }

    private void updateUsersFriendships() {
        for (Friendship friendship : friendshipRepository.findAll()) {
            User user1 = userRepository.findOne(friendship.getId().getFirst());
            User user2 = userRepository.findOne(friendship.getId().getSecond());
            user1.addFriend(user2);
            user2.addFriend(user1);
        }
    }

    public void addFriend(Long id1, Long id2) {
        User user1 = userRepository.findOne(id1);
        User user2 = userRepository.findOne(id2);
        if (user1 == null || user2 == null)
            throw new ServiceException("User does not exist,please input valid ids");

        if (friendshipRepository.findOne(new Pair(id1, id2)) != null || friendshipRepository.findOne(new Pair(id2, id1)) != null)
            throw new ServiceException("Friendship already exist,please input valid ids");

        user1.addFriend(user2);
        user2.addFriend(user1);
        Friendship friendship1 = new Friendship(new Pair<>(user1.getId(), user2.getId()));
        friendshipRepository.save(friendship1);
        userRepository.update(user1);
        userRepository.update(user2);


    }

    public void removeFriend(Long id1, Long id2) {
        User user1 = userRepository.findOne(id1);
        User user2 = userRepository.findOne(id2);
        if (user1 == null || user2 == null)
            throw new ServiceException("User does not exist,please input valid ids");

        if (friendshipRepository.findOne(new Pair(id1, id2)) == null && friendshipRepository.findOne(new Pair(id2, id1)) == null)
            throw new ServiceException("Friendship does not  exist,please input valid ids");

        user1.removeFriend(user2);
        user2.removeFriend(user1);
        friendshipRepository.delete(new Pair(user1.getId(), user2.getId()));
        friendshipRepository.delete(new Pair(user2.getId(), user1.getId()));
        userRepository.update(user1);
        userRepository.update(user2);
    }

    public Iterable<Friendship> findAll(){
        return friendshipRepository.findAll();
    }
}
