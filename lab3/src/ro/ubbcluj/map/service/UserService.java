package ro.ubbcluj.map.service;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.validators.FriendshipValidator;
import ro.ubbcluj.map.validators.ServiceException;
import ro.ubbcluj.map.validators.ValidationException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

//stocarea in fisier sau in memory a datelor pe care le primesti de la UI
public class UserService {
    private Repository<Long, User> userRepository;
    private Repository<Pair<Long>, Friendship> friendshipRepository;

    public UserService(Repository<Long, User> userRepository, Repository<Pair<Long>, Friendship> friendshipRepository) {
        this.userRepository = userRepository;
        this.friendshipRepository = friendshipRepository;
    }

    public void save(String firstName, String lastName) {
        Iterable<User> result = userRepository.findAll();
        User lastUser = null;
        for (User value : result) {
            lastUser = value;
        }

        User user = new User(firstName, lastName);
        if (lastUser != null) {
            user.setId(lastUser.getId() + 1);
        } else {
            user.setId(1L);
        }
        userRepository.save(user);
    }

    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    public void delete(Long id) {
        User deletedUser = userRepository.findOne(id);
        if (deletedUser == null)
            throw new ServiceException("No user with the given id");

        for (User user : deletedUser.getFriends()) {
            user.getFriends().removeIf(friend ->
                    Objects.equals(friend.getId(), deletedUser.getId())
            );
        }
        List<Friendship> friendshipList = new ArrayList<>();
        for (Friendship friendship : friendshipRepository.findAll()) {
            if (friendship.getId().getFirst() == id || friendship.getId().getSecond() == id) //conteaza daca prietenia ta e 1,2 sau 2,1, nu, tot o stergi
                friendshipList.add(friendship);
        }

        for (Friendship friendship : friendshipList) {
            friendshipRepository.delete(friendship.getId());
        }
        userRepository.delete(id);
    }


    public void update(Long id, String firstName, String lastName) {
        User user = new User(firstName, lastName);
        user.setId(id);
        userRepository.update(user);
    }

}
