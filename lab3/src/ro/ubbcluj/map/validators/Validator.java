package ro.ubbcluj.map.validators;

public interface Validator<T> {
    void validate(T entity) throws ValidationException;
}