package ro.ubbcluj.map.validators;

import ro.ubbcluj.map.domain.User;

public class UserValidator implements Validator<User> {
    @Override
    public void validate(User entity) throws ValidationException {
        String error = "";
        if(entity.getFirstName().equals("")){
            error += "First name must not be empty\n";
        }
        if(!entity.getFirstName().matches("[a-zA-Z]+")){
            error += "First name must contain only letters\n";
        }
        if(entity.getLastName().equals("")){
            error += "Last name must not be empty\n";
        }
        if(!entity.getLastName().matches("[a-zA-Z]+")){
            error += "Last name must contain only letters\n";
        }
        if(!error.equals(""))
            throw new ValidationException(error);
    }
}
