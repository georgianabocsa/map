package ro.ubbcluj.map;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.database.FriendshipDataBaseRepository;
import ro.ubbcluj.map.repository.database.UserDataBaseRepository;
import ro.ubbcluj.map.repository.file.FriendshipFileRepository;
import ro.ubbcluj.map.repository.file.UserFileRepository;
import ro.ubbcluj.map.service.ConnectionService;
import ro.ubbcluj.map.service.FriendshipService;
import ro.ubbcluj.map.service.UserService;
import ro.ubbcluj.map.ui.UI;
import ro.ubbcluj.map.validators.FriendshipValidator;
import ro.ubbcluj.map.validators.UserValidator;

public class Main {
    public static void main(String[] args) {
       //Repository<Long,User> userRepository = new InMemoryRepository<>(new UserValidator());
       //Repository<Pair<Long,Long>, Friendship> friendshipRepository = new InMemoryRepository<>(new FriendshipValidator());

       //Repository<Pair<Long>, Friendship>  friendshipRepo= new FriendshipFileRepository("data/friendship.in", new FriendshipValidator());
      // Repository<Long,User> userRepo = new UserFileRepository("data/user.in", new UserValidator());

       Repository<Long,User> userDataBaseRepo = new UserDataBaseRepository("jdbc:postgresql://localhost:5432/socialNetwork",
               "postgres","postgres",new UserValidator());
        Repository<Pair<Long>, Friendship> friendshipDataBaseRepo = new FriendshipDataBaseRepository("jdbc:postgresql://localhost:5432/socialNetwork",
                "postgres","postgres",new FriendshipValidator());


        UserService userService = new UserService(userDataBaseRepo,friendshipDataBaseRepo);
        FriendshipService friendshipService = new FriendshipService(userDataBaseRepo,friendshipDataBaseRepo);
        ConnectionService connectionService = new ConnectionService(userDataBaseRepo);
        UI ui =new UI(userService,friendshipService,connectionService);
        ui.startUI();
        System.out.println();

        System.out.println();

    }
}
