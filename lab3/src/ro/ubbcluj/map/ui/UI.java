package ro.ubbcluj.map.ui;

import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.service.ConnectionService;
import ro.ubbcluj.map.service.FriendshipService;
import ro.ubbcluj.map.service.UserService;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Scanner;

public class UI {
    private UserService userService;
    private FriendshipService friendshipService;
    private ConnectionService connectionService;

    public UI(UserService service, FriendshipService friendshipService,ConnectionService connectionService) {
        this.userService = service;
        this.friendshipService = friendshipService;
        this.connectionService = connectionService;
    }

    public String readCommand() {
        System.out.println("MENU:\n 1.Add User\n 2.Remove User\n 3.Add friend\n 4.Remove friend\n 5.Print communities\n" +
                " 6.The most social community\n 7.Print All Users \n 8.Print friendships\n 0.Exit\n");
        Scanner readObj = new Scanner(System.in);
        System.out.println("Give the command:\n");
        return readObj.nextLine();
    }

    public void printCommunities(){
        try {
            ArrayList<ArrayList<User>> components = connectionService.connectedComponents();
            System.out.println("Numarul de comunitati este: " + components.size());

            for (ArrayList<User> community : components) {
                System.out.println(community);

            }
            components.clear();
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

    }

    public void printTheMostSocialCommunity(){
        try {
            ArrayList<User> socialCommunity = connectionService.theMostSociableCommunity();
            System.out.println("The most social community has the following users:");
            for (User user : socialCommunity) {
                System.out.println(user);
            }
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

    }

    public void addUser() {
        Scanner readObj = new Scanner(System.in);
        System.out.println("Give the first name of the user:\n");
        String firstName = readObj.nextLine();
        System.out.println("Give the last name of the user:\n");
        String lastName = readObj.nextLine();
        try {
            userService.save(firstName, lastName);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public void removeUser() {
        Scanner readObj = new Scanner(System.in);
        System.out.println("Give the id of user you want to delete:\n");
        Long id = readObj.nextLong();
        try {
            userService.delete(id);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public void updateUser(){
        Scanner readObj = new Scanner(System.in);
        System.out.println("Give the id of user you want to update:\n");
        Long id = readObj.nextLong();
        String endOfLine = readObj.nextLine();
        System.out.println("Give the first name of the user:\n");
        String firstName = readObj.nextLine();
        System.out.println("Give the last name of the user:\n");
        String lastName = readObj.nextLine();
        try{
            userService.update(id,firstName,lastName);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

    }

    public void addFriend() {
        Scanner readObj = new Scanner(System.in);
        System.out.println("Give the id of the user to whom you want to add a friend\n");
        Long id1 = readObj.nextLong();
        System.out.println("Give the id of user you want to add as a friend:\n");
        Long id2 = readObj.nextLong();
        try {
            friendshipService.addFriend(id1, id2);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public void removeFriend() {
        Scanner readObj = new Scanner(System.in);
        System.out.println("Give the id of the user to whom you want to remove a friend\n");
        Long id1 = readObj.nextLong();
        System.out.println("Give the id of user you want to a remove:\n");
        Long id2 = readObj.nextLong();
        try {
            friendshipService.removeFriend(id1, id2);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

    }

    public void printAllUsers() {
        System.out.println(userService.findAll());
    }

    public void printAllFriendships(){
        System.out.println(friendshipService.findAll());
    }

    public void startUI() {
        boolean ok = true;
        while (ok) {
            String command = readCommand();
            switch (command) {
                case "1" -> addUser();
                case "2" -> removeUser();
                case "3" -> addFriend();
                case "4" -> removeFriend();
                case "5" -> printCommunities();
                case "6" -> printTheMostSocialCommunity();
                case "7" -> printAllUsers();
                case "8" -> printAllFriendships();
                case "0" -> ok =false;
                default -> System.out.println("Invalid command\n");
            }
        }
    }

}
